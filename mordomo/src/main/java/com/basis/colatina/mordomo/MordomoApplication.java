package com.basis.colatina.mordomo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MordomoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MordomoApplication.class, args);
	}

}
