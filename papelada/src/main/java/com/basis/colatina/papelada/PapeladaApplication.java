package com.basis.colatina.papelada;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PapeladaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PapeladaApplication.class, args);
	}

}
